﻿using System;
using System.Numerics;
using Dalamud.Interface.Windowing;
using ImGuiNET;

namespace SimpleTodo.Windows;

public sealed class ConfigWindow : Window, IDisposable
{
    private readonly Plugin _plugin;
    private readonly Configuration _config;

    public ConfigWindow(Plugin plugin) : base("Simple Todo Configuration###config")
    {
        Flags = ImGuiWindowFlags.NoResize | ImGuiWindowFlags.NoCollapse | ImGuiWindowFlags.NoScrollbar | ImGuiWindowFlags.NoScrollWithMouse;

        Size = new Vector2(250, 175);

        _plugin = plugin;
        _config = plugin.Configuration;
    }

    public override void Draw()
    {
        ImGui.Text(Translation.config_done_header);

        ImGui.Spacing();

        var doneTodoAction = (int)_config.DoneTodoAction;
        if (ImGui.RadioButton(Translation.config_nothing, ref doneTodoAction, 0))
        {
            _config.DoneTodoAction = DoneTodoAction.Nothing;
            _config.Save();
        }

        ImGui.Spacing();

        if (ImGui.RadioButton(Translation.config_hide, ref doneTodoAction, 1))
        {
            _config.DoneTodoAction = DoneTodoAction.Hide;
            _config.Save();
        }

        ImGui.Spacing();

        if (ImGui.RadioButton(Translation.config_remove, ref doneTodoAction, 2))
        {
            _config.DoneTodoAction = DoneTodoAction.Remove;
            _config.Save();
        }

        ImGui.Spacing();

        var languageOverride = (int)_config.LanguageOverride;
        if (ImGui.Combo(Translation.language_language, ref languageOverride, _config.LanguageNames, _config.LanguageNames.Length))
        {
            _config.LanguageOverride = (LanguageOverride)languageOverride;
            _config.Save();
            _plugin.OnLanguageChanged(_config.LanguageOverride.Code());
        }
    }

    public void Dispose() { }
}
