﻿using System;
using System.Collections.Generic;
using System.Numerics;
using Dalamud;
using Dalamud.Interface;
using Dalamud.Interface.Components;
using Dalamud.Interface.Windowing;
using ImGuiNET;

namespace SimpleTodo.Windows;

public sealed class MainWindow : Window, IDisposable
{
    private readonly Vector2 ButtonSize = new(28);

    private readonly Plugin _plugin;
    private readonly List<SimpleTodoEntry> _list;

    private string _currentText = "";

    private bool _isInDisplayMode = true;

    public MainWindow(Plugin plugin) : base("Simple Todo##main")
    {
        _plugin = plugin;
        _list = plugin.Configuration.SimpleTodos;

        SizeConstraints = new WindowSizeConstraints
        {
            MinimumSize = new Vector2(200, 100)
        };

        TitleBarButtons.Add(new TitleBarButton
        {
            Icon = FontAwesomeIcon.Cog,
            Click = (_) => plugin.ToggleConfigUI(),
        });

        if (plugin.Configuration.IsFirstTimeOpen)
        {
            _list.Add(new SimpleTodoEntry(Translation.main_sample));
            plugin.Configuration.IsFirstTimeOpen = false;
            plugin.Configuration.Save();
        }
    }

    public override void Draw()
    {
        RenderGeneralActions();

        ImGui.Spacing();
        ImGui.Spacing();

        RenderTodoList();

        RenderNewTodoForm();
    }

    private void RenderNewTodoForm()
    {
        if (!_isInDisplayMode)
        {
            if (ImGui.InputText("", ref _currentText, 127, ImGuiInputTextFlags.EnterReturnsTrue))
            {
                AddEntry(_currentText);
            }

            ImGui.SameLine();

            if (ImGuiComponents.IconButton(FontAwesomeIcon.Plus))
            {
                AddEntry(_currentText);
            }
        }
    }

    private void RenderTodoList()
    {
        for (var index = 0; index < _list.Count; index++)
        {
            var entry = _list[index];
            if (_plugin.Configuration.DoneTodoAction == DoneTodoAction.Hide && entry.IsDone)
            {
                continue;
            }

            RenderTodoToggle(index, entry);

            ImGui.SameLine();

            RenderTodoRemove(index, entry);

            ImGui.SameLine();

            RenderTodoText(entry);

            ImGui.Spacing();
        }
    }

    private void RenderTodoText(SimpleTodoEntry entry)
    {
        if (entry.IsDone)
            ImGui.TextDisabled(entry.Text);
        else
            ImGui.Text(entry.Text);
    }

    private void RenderTodoRemove(int index, SimpleTodoEntry entry)
    {
        if (!_isInDisplayMode && ImGuiComponents.IconButton(index, FontAwesomeIcon.Trash))
        {
            RemoveEntry(entry);
        }
    }

    private void RenderTodoToggle(int index, SimpleTodoEntry entry)
    {
        ImGui.BeginDisabled(!_isInDisplayMode);

        var isDone = entry.IsDone;
        if (ImGui.Checkbox("###toggle" + index, ref isDone))
        {
            if (_plugin.Configuration.DoneTodoAction == DoneTodoAction.Remove && !entry.IsDone)
                RemoveEntry(entry);
            else
                ToggleEntry(entry);
        }

        ImGui.EndDisabled();
    }

    private void RenderGeneralActions()
    {
        ImGui.PushFont(UiBuilder.IconFont);

        var icon = _isInDisplayMode ? FontAwesomeIcon.Pen : FontAwesomeIcon.Times;
        if (ImGui.Button(icon.ToIconString(), ButtonSize))
        {
            _isInDisplayMode = !_isInDisplayMode;
            _currentText = "";
        }

        ImGui.PopFont();

        ImGui.SameLine();

        ImGui.BeginDisabled(!ImGui.IsKeyDown(ImGuiKey.LeftShift));

        if (ImGuiComponents.IconButton(FontAwesomeIcon.TrashAlt))
        {
            _list.Clear();
            Save();
        }

        ImGui.EndDisabled();

        if (ImGui.IsItemHovered(ImGuiHoveredFlags.AllowWhenDisabled))
        {
            ImGui.SetTooltip(Translation.main_tooltip);
        }
    }

    private void AddEntry(string saveText)
    {
        _list.Add(new SimpleTodoEntry(saveText));
        Save();
        _currentText = "";
    }

    private void ToggleEntry(SimpleTodoEntry entry)
    {
        var index = _list.IndexOf(entry);
        _list.RemoveAt(index);
        _list.Insert(index, entry with { IsDone = !entry.IsDone });
        Save();
    }

    private void RemoveEntry(SimpleTodoEntry entry)
    {
        _list.Remove(entry);
        Save();
    }

    private void Save()
    {
        _plugin.Configuration.SimpleTodos = _list;
        _plugin.Configuration.Save();
    }

    public void Dispose() { }
}
