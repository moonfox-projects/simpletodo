﻿using Dalamud.Configuration;
using Dalamud.Plugin;
using System;
using System.Collections.Generic;

namespace SimpleTodo;

[Serializable]
public sealed class Configuration : IPluginConfiguration
{
    public int Version { get; set; } = 0;

    public LanguageOverride LanguageOverride { get; set; } = LanguageOverride.None;
    public DoneTodoAction DoneTodoAction { get; set; } = DoneTodoAction.Nothing;

    public List<SimpleTodoEntry> SimpleTodos = [];

    public bool IsFirstTimeOpen { get; set; } = true;

    [NonSerialized]
    public string[] LanguageNames =
    [
        Translation.language_game,
        Translation.language_japanese,
        Translation.language_english,
        Translation.language_german,
        Translation.language_french
    ];

    public void ReloadLanguageNames()
    {
        LanguageNames[0] = Translation.language_game;
        LanguageNames[1] = Translation.language_japanese;
        LanguageNames[2] = Translation.language_english;
        LanguageNames[3] = Translation.language_german;
        LanguageNames[4] = Translation.language_french;
    }

    [NonSerialized]
    private IDalamudPluginInterface? PluginInterface;

    public void Initialize(IDalamudPluginInterface pluginInterface)
    {
        PluginInterface = pluginInterface;
    }

    public void Save()
    {
        PluginInterface!.SavePluginConfig(this);
    }
}

public enum LanguageOverride
{
    None,
    Japanese,
    English,
    German,
    French
}

public static class Extensions
{
    public static string Code(this LanguageOverride lang) =>
        lang switch
        {
            LanguageOverride.None => "",
            LanguageOverride.Japanese => "ja",
            LanguageOverride.English => "en",
            LanguageOverride.German => "de",
            LanguageOverride.French => "fr",
            _ => ""
        };
}

public enum DoneTodoAction
{
    Nothing,
    Hide,
    Remove
}

public record struct SimpleTodoEntry(string Text, bool IsDone = false);
