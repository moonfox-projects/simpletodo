﻿using System.Globalization;
using Dalamud.Game.Command;
using Dalamud.Plugin;
using Dalamud.Interface.Windowing;
using Dalamud.Plugin.Services;
using SimpleTodo.Windows;

namespace SimpleTodo;

// ReSharper disable once ClassNeverInstantiated.Global
public sealed class Plugin : IDalamudPlugin
{
    private const string MainCommandName = "/ptodo";
    private const string ConfigCommandName = "/ptodoconfig";

    public IDalamudPluginInterface PluginInterface { get; init; }
    private ICommandManager CommandManager { get; init; }
    public Configuration Configuration { get; init; }

    public readonly WindowSystem WindowSystem = new("SimpleTodo");
    private MainWindow MainWindow { get; init; }
    private ConfigWindow ConfigWindow { get; init; }

    public Plugin(
        IDalamudPluginInterface pluginInterface,
        ICommandManager commandManager)
    {
        PluginInterface = pluginInterface;
        CommandManager = commandManager;

        Configuration = PluginInterface.GetPluginConfig() as Configuration ?? new Configuration();
        Configuration.Initialize(PluginInterface);
        OnLanguageChanged(PluginInterface.UiLanguage);

        MainWindow = new MainWindow(this);
        ConfigWindow = new ConfigWindow(this);

        WindowSystem.AddWindow(MainWindow);
        WindowSystem.AddWindow(ConfigWindow);

        CommandManager.AddHandler(MainCommandName, new CommandInfo((_, _) => ToggleMainUI())
        {
            HelpMessage = Translation.plugin_main
        });

        CommandManager.AddHandler(ConfigCommandName, new CommandInfo((_, _) => ToggleConfigUI())
        {
            HelpMessage = Translation.plugin_config
        });

        PluginInterface.UiBuilder.Draw += DrawUI;

        PluginInterface.UiBuilder.OpenConfigUi += ToggleConfigUI;

        PluginInterface.UiBuilder.OpenMainUi += ToggleMainUI;

        PluginInterface.LanguageChanged += OnLanguageChanged;
    }

    public void OnLanguageChanged(string langCode)
    {
        var code = Configuration.LanguageOverride == LanguageOverride.None
                       ? langCode
                       : Configuration.LanguageOverride.Code();
        
        Translation.Culture = new CultureInfo(code);
        
        Configuration.ReloadLanguageNames();
    }

    public void Dispose()
    {
        PluginInterface.LanguageChanged -= OnLanguageChanged;

        WindowSystem.RemoveAllWindows();

        MainWindow.Dispose();
        ConfigWindow.Dispose();

        CommandManager.RemoveHandler(MainCommandName);
        CommandManager.RemoveHandler(ConfigCommandName);
    }

    private void DrawUI() => WindowSystem.Draw();

    public void ToggleMainUI() => MainWindow.Toggle();
    public void ToggleConfigUI() => ConfigWindow.Toggle();
}
