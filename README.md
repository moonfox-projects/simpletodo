# SimpleTodo
This Plugin aims to provide a simple Todo List, so you can easily keep track of everything you might want to do c:

## Commands
`/ptodo` will open the Todo List \
`/ptodoconfig` will open the configuration, so you can:
- Choose what to do with Entries that are marked as Done
  - Nothing
  - Hide
  - Remove

This plugin is fully translated into the main languages of FFXIV, namely Japanese, English, German and French.

I hope you find this plugin useful! I am absolutely open to any Feedback or ideas for improvement
